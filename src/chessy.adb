with clic ;
with board ;
with board.bishop ;

procedure Chessy is
   g : board.Game := board.Create(6) ;
   b : board.PiecePtr := board.Bishop.Create ;
   loc : board.location := ( row => 3 , col => 3 );
begin
   board.Show(g) ;
   board.Place(b , g , loc );
   board.Show(g) ;
end Chessy;
