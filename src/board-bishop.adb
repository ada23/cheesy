package body board.bishop is
    function Create return PiecePtr is 
    begin
        return new BishopPiece ;
    end Create ;

    function Name( p : BishopPiece ) return String is
    begin 
        return "bishop" ;
    end Name ;

    procedure Place( p : access BishopPiece ; g : in out Game ; loc : location ) is 
       procedure Influence( r : Integer ; c : Integer ) is
       begin
           if r in 1..g.size and c in 1..g.size
           then
              g.sq(r,c).s := SystemInfluenced ;
           end if ;
       end Influence ;

    begin
       if g.sq(loc.row,loc.col).s /= Available
       then
          raise Program_Error ;
       end if ;
       g.sq(loc.row,loc.col).s := Occupied ;
       g.sq(loc.row,loc.col).p := p ;
       for n in 1..g.size
       loop
          Influence( loc.row + n , loc.col + n);
          Influence( loc.row - n , loc.col + n);
          Influence( loc.row + n , loc.col - n);
          Influence( loc.row - n , loc.col - n);
       end loop ;
    end Place ;

end board.bishop ;
