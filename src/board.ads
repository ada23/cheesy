package board is
    type Color is ( Black , White );
    type State is ( Available , Occupied , SystemInfluenced , PlayerInfluenced ) ;
    type location is record
        row : integer := 0 ;
        col : integer := 0 ;
    end record ;
    type piece is abstract tagged record 
        c : color := Black ;
        loc : location ;
    end record ;   
    type PiecePtr is access all Piece'Class ;

    type Square is record
        c : Color := Black ;
        s : State := Available ;
        p : access Piece'Class ;
    end record;

    type Board is array (integer range <>, integer range <> ) of Square ;
    type Game ( size : integer ) is record
       sq : Board(1..size,1..size) ;
    end record ;

    function Create (s : integer) return Game ;
    procedure Show( g : Game );

    function Create return PiecePtr is abstract ;
    function Name( p : piece ) return String is abstract ;
    procedure Place( p : access piece ; g : in out Game ; loc : location ) is abstract ;

end board ;