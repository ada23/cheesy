package board.bishop is
    type BishopPiece is new Piece with null record ;
    function Create return PiecePtr  ;
    function Name( p : BishopPiece ) return String  ;
    procedure Place( p : access BishopPiece ; g : in out Game ; loc : location ) ;
end board.bishop ;
