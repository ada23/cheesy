with Text_Io; use Text_Io ;

package body board is
    function Create (s : integer) return Game is
       result : Game(s) ;
       nextcolor : Color := Black ;
    begin
       if s mod 2 /= 0
       then
          raise Program_Error ;
       end if ;
       for r in 1..s
       loop
          for c in 1..s
          loop
            result.sq(r,c).c := nextcolor ;
            case nextcolor is
               when Black => nextcolor := White ;
               when White => nextcolor := Black ;
            end case ;
          end loop ;
          case nextcolor is
               when Black => nextcolor := White ;
               when White => nextcolor := Black ;
          end case ;
       end loop ;
       return result ;
    end Create ;
    procedure Show( g : Game ) is
    begin
        for c in 1..g.size
        loop
            Put("+---") ;
        end loop ;
        Put_Line("+") ;

        for r in 1..g.size
        loop
           for c in 1..g.size
           loop
              case g.sq(r,c).c is
                 when Black => Put("| . ") ;
                 when White => Put("|   ") ;
              end case ;
           end loop ;
           Put_Line("|") ;
           for c in 1..g.size
           loop
              Put("+---") ;
           end loop ;
           Put_Line("+") ;
        end loop ;
    end Show ;
end board ;
